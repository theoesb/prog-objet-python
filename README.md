# Entraînement à la programmation orienté objet avec Python

# Objectif

Créer une classe "Humain" et une classe "Membre" et faire en sorte qu'un humain puisse devenir un membre

Un Humain aura :

* Un nom
* Un âge

Un Membre aura :

* Un Id unique
* Un pseudo
