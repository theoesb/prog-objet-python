class Humain:
    """Ceci est la classe Humain"""

    humains = []
    nb = 0

    def __init__(self):
        self.nom = None
        self.age = None

    def creation(self, nom, age):
        print("Création d'Humain....")
        Humain.nb += 1
        self.nom = nom
        self.age = age
        Humain.humains.append((nom, age))

    @classmethod
    def affiche_info_humain(cls):
        print("")
        print("Il y a maintenant", Humain.nb, "Humains :")
        for nom, age in cls.humains:
            print("-----------")
            print("Nom :", nom)
            print("Âge :", age)


class Membre(Humain):
    membres = []
    nb = 0

    def __init__(self):
        super().__init__()
        self.pseudo = None

    def adhesion(self, pseudo, nom, age):
        print("Adhésion de votre Humain....")
        print("")
        self.pseudo = pseudo
        id = Membre.nb
        Membre.nb += 1
        Membre.membres.append((id, pseudo, nom, age))

    @classmethod
    def affiche_info_membre(cls):
        print("")
        print("Il y a maintenant", Membre.nb, "Membres :")
        for id, pseudo, nom, age in cls.membres:
            print("------------")
            print("Id :", id)
            print("Pseudo :", pseudo)
            print("Nom :", nom)
            print("Âge :", age)


def ajout_humain():
    nom = input("Entrez le nom de votre Humain : ")
    age = input("Entrez l'âge de votre Humain : ")
    print("")
    reponse = input("Voulez-vous que votre Humain soit un membre ? (o/n) : ")
    if reponse.lower() == "o":
        pseudo = input("Entrez un pseudo pour votre Humain : ")
        ajout = Membre()
        ajout.creation(nom, age)
        ajout.adhesion(pseudo, nom, age)
    elif reponse.lower() == "n":
        ajout = Humain()
        ajout.creation(nom, age)


ajout_humain()
ajout_humain()
Humain.affiche_info_humain()
Membre.affiche_info_membre()

